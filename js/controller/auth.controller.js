(function () {
    'use strict';

angular
	.module('login2StepsApp')
		.controller('authCtrl',  authCtrl);


	authCtrl.$inject = ['$scope', '$routeParams', 'authservice'];

	function authCtrl($scope, $routeParams, authservice){
		
		var vm = this; 

		/*
		* Champ identifiant à controller
		*/
		$scope.username = "";

		/*
		* Champ identifiant à controller
		*/
		$scope.password = "";

		/*
		* Erreur sur l'identifiant
		*/
		vm.erreurLogin = false;

		/*
		* Message de l'erreur
		*/
		vm.message = "";

		/*
		* Gestion de l'affichage 
		* Décalage pour afficher un champ ou un autre
		*/
		vm.right = false;

		/*
		* Gestion des animations à l'arrivée
		*/
		vm.starter = 0;

		/*
		* Informe l'utilisateur d'un chargement de données en cours
		*/
		vm.ajaxSend = false;

		/*
		* Fonction de déconnéxion
		*/
		vm.logout = logout;

		/*
		* Fonction de vérification de l'identifiant
		*/
		vm.checkIdentifiant = checkIdentifiant;

		/*
		* Fonction de vérification du mot de passe
		*/
		vm.checkPassword = checkPassword;

		/*
		* Image affichée pour l'utilisateur si connecté
		* sinon image par défault
		*/
		vm.pathImg = "account.png";

		/*
		* Fonction de vérification des champs
		*/
		vm.slideRight = slideRight;

		/*
		*  Fonction affichant une erreur en cas d'échec de la requête
		*/
		vm.error = error;


		function logout(){

			function successCallbackFunction(response){

			    	if(response.data['message'] == "deconnecte"){
			    		 Materialize.toast("Vous êtes maintenant déconnecté", 3000, 'green white-text center-align');
			    		 $scope.authenticate = false;
			    	}
			    	else{
			    		Materialize.toast("Erreur lors de la déconnexion", 3000, 'orange accent-2 white-text center-align');
			    		angular.element('a[ng-search-page]').triggerHandler('click');
			    	}

		 		}


			
			var promise = authservice.logout();

			promise.then(
				function successCallBack(response){
				   successCallbackFunction(response);
				}

				, function errorCallback(response) {
				    	vm.error();
				  });
		}

		function checkIdentifiant($event){

			var check = true;
			var log = $scope.username;

			if(log == "" ||
				log.length < 4 ){
				check = false;
			}

			if(!check){
				$event.preventDefault();
				$event.stopPropagation();
				vm.erreurLogin = true;
				vm.message =  "Identifiant incorrect"
			}
			else{

				var response = authservice.verifyLogin($scope.username);

				if(response.auth){
					vm.pathImg = response.img;
					vm.erreurLogin = false;
					vm.message = "";
					vm.starter = 1;
					vm.right = true;
				}
				else{
					vm.erreurLogin = true;
					vm.message = "Identifiant inconnu";
				}

			}

			return check;
		}

		function checkPassword($event){

			var check = true;
			var pass = $scope.password;

			if(pass == "" ||
				pass.length < 4 ){
				check = false;
			}

			if(!check){
				$event.preventDefault();
				$event.stopPropagation();
				vm.erreurPassword = true;
				vm.message =  "Mot de passe incorrect"
			}
			else{
				var response = authservice.connect($scope.username,$scope.password);
				if(response){
					vm.erreurPassword = false;
					vm.message = "";
					vm.right = true;
					Materialize.toast('Vous êtes désormais connecté(e)', 3000, 'green center-align');
				}
				else{
					vm.message = "Mot de passe incorrect";
					vm.erreurPassword = true;
				}
			}

			return check;
		}

		function error(){
			$scope.ajaxSend = false;
			Materialize.toast('Une erreur est survenue, veuillez réessayer ultérieurement', 3000, 'red accent-2 center-align');
		}

		function slideRight(){
			vm.right = false;
			$scope.password = "";
			$scope.username = "";
			vm.pathImg =  "account.png";
		}
	}

})();