(function(){

	angular
		.module("login2StepsApp")
			.factory('requestservice', requestservice);

			requestservice.$inject = ['$http'];

		function requestservice(){
			/*
			* Object contenant les functions et propriétés
			*/
			var service = {
				sendRequest : sendRequest
			};

			/*
			* Service d'envoi de reqêtes asynchrones
			*/
			function sendRequest(method, url, params){

				return $http({
					method: method,
					url: url,
					params: params
				});
			}

			return service;
	}

})();