
(function(){

	angular
		.module('login2StepsApp')
			.factory('authservice', authservice);

			authservice.$inject = ['$location', 'requestservice'];

		function authservice($location, requestservice){


				/*
				* Liste des utilisateur autorisé (login)
				* et des mots de passe
				* Non cryptés bsr !!!
				* et des liens vers leur image de profil
				*/
				var users = [
					{
						login: "test",
						password: "12345",
						profilImg: "login.jpg"
					},
					{
						login: 'admin',
						password: "admin!",
						profilImg: "admin.jpg"
					}
				];

				var service = {
					connect : connect,
					logout  : logout,
					verifyLogin: verifyLogin
				};



			 	function connect(login, password){

			 		var vretour = false;

				 	angular.forEach(users, function(user, $index){
				 		if(user.login == login
				 			&& user.password == password){
				 			vretour = true;
				 		}
				 	}, []);

				 	return vretour;	
			 			 	
				}

				function verifyLogin(login){
					var vretour = {
						auth: false,
						img: ""
					};

				 	angular.forEach(users, function(user, $index){
				 		if(user.login == login){
				 			vretour.auth = true;
				 			vretour.img = user.profilImg;
				 		}
				 	}, []);

				 	return vretour;	 	
				}

				function logout(){

			 	return requestservice.sendRequest("GET", "/logoutUser", {

				});	
			}

		return service;
	}

})();

